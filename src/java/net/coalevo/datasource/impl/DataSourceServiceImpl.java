/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.datasource.model.DataSourceProvider;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;

import java.util.NoSuchElementException;

/**
 * This class implements {@link DataSourceService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class DataSourceServiceImpl
    implements DataSourceService {

  private DataSourceProviderManager m_DSPManager;

  public void activate(BundleContext bc) {
    //1. Activate provider manager
    m_DSPManager = new DataSourceProviderManager();
    m_DSPManager.activate(bc);
  }//activate

  public void deactivate() {
    m_DSPManager.deactivate();
    m_DSPManager = null;
  }//deactivate

  public DataSource getDataSource(String id) {
    DataSourceProvider dsp = m_DSPManager.get(id);
    if(dsp != null) {
      return dsp.getDataSource();
    } else {
      throw new NoSuchElementException();
    }
  }//getDataSource

  public boolean isAvailableDataSource(String id) {
    return m_DSPManager.isAvailable(id);
  }//isAvailableDataSource

  public DataSource waitForDataSource(String id, long time) {
    DataSourceProvider dsp = m_DSPManager.waitFor(id,time);
     if(dsp != null) {
      return dsp.getDataSource();
    } else {
      throw new NoSuchElementException();
    }
  }//waitForDataSource

}//class DataSourceServiceImpl
