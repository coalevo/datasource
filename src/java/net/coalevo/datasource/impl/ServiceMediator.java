/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.impl;

import net.coalevo.foundation.service.MessageResourceService;
import org.osgi.framework.*;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * This class implements a service mediator for this bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());
  private BundleContext m_BundleContext;
  private MessageResourceService m_MessageResourceService;

  private CountDownLatch m_MessageResourceServiceLatch;

  public ServiceMediator() {

  }//constructor

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_MessageResourceServiceLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(objectclass=" + MessageResourceService.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_MessageResourceService = null;

    if(m_MessageResourceServiceLatch != null) {
      m_MessageResourceServiceLatch.countDown();
      m_MessageResourceServiceLatch = null;
    }

    m_BundleContext = null;
    m_LogMarker = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          }  else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
