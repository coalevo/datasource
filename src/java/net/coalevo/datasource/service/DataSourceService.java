/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.service;

import javax.sql.DataSource;
import java.util.NoSuchElementException;

/**
 * This interface defines the <tt>DataSourceService</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface DataSourceService {

  /**
   * Returns the <tt>DataSource</tt> mapped to the given
   * identifier.
   *
   * @param id the identifier.
   * @return a <tt>DataSource</tt>.
   *
   * @throws NoSuchElementException if such a <tt>DataSource</tt>
   *         does not exist.
   */
  public DataSource getDataSource(String id);

  /**
   * Tests if the <tt>DataSource</tt> mapped to the given
   * identifier is available.
   *
   * @param id the identifier.
   * @return true if available, false otherwise.
   */
  public boolean isAvailableDataSource(String id);

  /**
   * Waits for a <tt>DataSource</tt> the specified time.
   * <p/>
   * If the <tt>DataSource</tt> is available, then it will
   * be returned immediately, otherwise the thread will
   * be waiting for it to arrive, max. up to the given time.
   * If the <tt>DataSource</tt> with the given identifier is not
   * available after the given time, the method will throw a
   * <tt>NoSuchElementException</tt>.
   * <p/>
   * A time of -1 indicates that waiting should be unlimited.
   *
   * @param id the identifier.
   * @param time the maximum time to wait for the <tt>DataSource</tt>.
   * @return a <tt>DataSource</tt>.
   * @throws NoSuchElementException if such a <tt>DataSource</tt>
   *         does not exist and did not become available within the max. waiting time.
   */
  public DataSource waitForDataSource(String id, long time);

}//interface DataSourceService
