/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.model;

import javax.sql.DataSource;

/**
 * This interface defines a <tt>DataSourceProvider</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface DataSourceProvider {

  /**
   * Return the identifier of this <tt>DataSourceProvider</tt>.
   * @return the identifier as String.
   */
  public String getIdentifier();

  /**
   * Returns a <tt>DataSource</tt>, as provided
   * by the <tt>DataSourceProvider</tt> implementation.
   *
   * @return a <tt>DataSource</tt>.
   */
  public DataSource getDataSource();

}//interface DataSourceProvider
